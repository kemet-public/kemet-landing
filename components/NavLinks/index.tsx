import Link from "next/link";
import { useRouter } from "next/router";
import { FunctionComponent, useCallback } from "react";
import styles from './NavLinks.module.css'

interface NavLinksProps {

}

const NavLinks: FunctionComponent<NavLinksProps> = () => {
    const router = useRouter();
    const navLinks = [
        { 
            name: "Home", 
            path: "/" 
        },
        {
          name: "About Us",
          path: "/about",
        },
        {
          name: "Team",
          path: "/team",
        }
    ];
    const isActive = useCallback((item: string) => {
        return router.pathname === item
    },[router.pathname])
    return (
        <div className={styles.navLinksContainer}>
            {
                navLinks.map(link => (
                    <Link key={link.path} href={link.path} className={`${styles.link} ${isActive(link.path.substring(1)) ? styles.active : '' }`}>
                        {link.name}
                    </Link>
                ))
            }
            <span className={styles.line}></span>
        </div>
    );
}

export default NavLinks;