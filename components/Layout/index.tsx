import { FunctionComponent } from "react";
import styles from './Layout.module.css'

interface LayoutProps {
    children: React.ReactNode;
}

const Layout: FunctionComponent<LayoutProps> = ({ children }) => {
    return (<div className={styles.bodyContainer}>
        {children}
    </div>);
}

export default Layout;