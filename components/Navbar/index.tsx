import Image from "next/image";
import { FunctionComponent, Component } from "react";
import styles from './Navbar.module.css'
import Logo from '../../assets/logo.svg'
import IG from '../../assets/svg/IG.svg'
import Facebook from '../../assets/svg/Facebook.svg'
import Twitter from '../../assets/svg/Twitter.svg'

interface NavbarProps {
    
}
 
const Navbar: FunctionComponent<NavbarProps> = () => {
    return ( <nav className={styles.nav}>
        <div className={styles.logo}>
            <Image src={Logo} />
        </div>
        <div className={styles.social}>
            <Image src={IG} />
            <Image src={Facebook} />
            <Image src={Twitter} />
            {/* <IG />
            <Facebook />
            <Twitter /> */}
        </div>
        <div className={styles.contact}>
            Contact Us
        </div>
        {/* menu icon  & all data inside */}
        {/* social media container + contact us + list of pages */}
      </nav> );
}
 
export default Navbar;