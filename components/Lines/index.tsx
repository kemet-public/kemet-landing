import { FunctionComponent } from "react";
import { motion } from 'framer-motion';
import styles from './Lines.module.css'

interface LinesProps {
    
}
 
const Lines: FunctionComponent<LinesProps> = () => {
    return ( <div className={styles.linesContainer}>
        <motion.div className={styles.line1}></motion.div>
        <motion.div animate={{
          top: 0,
          transition: {
            duration: 1.9
          }
        }} className={styles.line2}></motion.div>
        <motion.div animate={{
          top: 0,
          transition: {
            duration: 1.5
          }
        }} className={styles.line3}></motion.div>
        <motion.div animate={{
          top: 0,
          transition: {
            duration: 1.2
          }
        }} className={styles.line4}></motion.div>
        <motion.div animate={{
          top: 0,
          transition: {
            duration: 1
          }
        }} className={styles.line5}></motion.div>
      </div> );
}
 
export default Lines;