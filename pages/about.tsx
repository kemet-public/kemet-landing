import type { NextPage } from 'next'
import Head from 'next/head'
import { motion } from 'framer-motion'
import styles from '../styles/Home.module.css'
import Navbar from '../components/Navbar'
import Lines from '../components/Lines'
import Link from 'next/link'
import Layout from '../components/Layout'

const About: NextPage = () => {
    return (
        <div>
            <Head>
                <title>Kemet Trading</title>
                <meta name="Kemet Trading" content="Kemet Trading" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main className={styles.mainContainer}>
                <Layout>
                    <Navbar />
                    <Link href="/">
                        <a>
                            hiiiii
                        </a>
                    </Link>
                </Layout>
                <Lines />
            </main>
        </div>
    )
}

export default About
